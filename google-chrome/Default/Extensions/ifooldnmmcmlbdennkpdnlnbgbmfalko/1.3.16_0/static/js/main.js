$(function() {
  var ext = chrome.extension.getBackgroundPage(),
    $min = $("#minutes"),
    $sec = $("#seconds"),
    $colors = $("#colors"),
    swapButtons = function() {
      $("#start,#stop").toggle();
    };
  $colors.prop("checked", ext.timers.colorsOn);
  $colors.on("click", function() {
    ext.timers.colorsOn = $(this).is(":checked");
  });
  chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
    var timer = ext.timers.get(tabs[0].id);
    if (timer) {
      swapButtons();
      var min = timer.interval / (60 * 1000);
      $min.val(Math.floor(min));
      $sec.val(Math.round((min - Math.floor(min)) * 60));
    } else {
      $min.val(localStorage.defaultMin || 0);
      $sec.val(localStorage.defaultSec || 15);
    }
  });

  $("#start").on("click", function() {
    chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
      var interval = $min.val() * 60 * 1000 + $sec.val() * 1000;
      ext.timers.set(tabs[0], interval);
    });
    swapButtons();
  });

  $("#stop").on("click", function() {
    chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
      ext.timers.remove(tabs[0].id);
    });
    swapButtons();
  });

  $(".options-link").on("click", function() {
    chrome.runtime.openOptionsPage();
  });

  $(".number-input").on("keydown", function(e) {
    if (e.keyCode === 109 || e.keyCode === 189 || e.keyCode === 190) {
      return false;
    }
  });

  setTimeout(function() {
    $sec.focus()[0].select();
  }, 100);
});
