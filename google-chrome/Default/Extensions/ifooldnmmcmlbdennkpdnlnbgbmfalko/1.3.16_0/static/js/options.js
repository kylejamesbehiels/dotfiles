$(function() {
  let ext = chrome.extension.getBackgroundPage();
  let timers = chrome.extension.getBackgroundPage()._timers;
  let initialTimers;
  let pausedTimers = {};
  let reqStorage = {};
  let reqAll = {};
  const requestFilters = {
    urls: ["<all_urls>"],
    types: ["main_frame"]
  };
  init();
  getRemainingTimeLoop();
  updateTimersLive();

  async function init() {
    initialTimers = JSON.parse(JSON.stringify(timers));
    renderTimers();
    setDefaultTime();
  }

  function setDefaultTime() {
    const min = localStorage.getItem("defaultMin") || 0;
    const sec = localStorage.getItem("defaultSec") || 15;
    $("#minutes").val(min);
    $("#seconds").val(sec);
  }

  $("#seconds, #minutes").on("change", () => {
    const min = parseInt($("#minutes").val());
    const sec = parseInt($("#seconds").val());
    localStorage.setItem("defaultMin", min);
    localStorage.setItem("defaultSec", sec);
  });

  function getRemainingTimeLoop() {
    setInterval(function() {
      for (key in timers) {
        let timer = timers[key];
        $(`.remaining-${timer.tab.index}`).text(
          calculateTime(timer.timeLeft._i)
        );
        if (timer.duration) {
          $(`.duration-${timer.tab.index}`).text(`${timer.duration} ms`);
        }
      }
    }, 1000);
  }

  function renderTimers() {
    $(".timer-row").remove();
    let $el = "";
    for (key in timers) {
      $("#data").append(
        "<tr class='tr-load'><td colspan='5' class='data-empty loading'>Loading ...</td></tr>"
      );
    }
    for (key in timers) {
      let timer = timers[key];
      $el += `<tr class='timer-row timer-${key}' data-tab=${timer.tab.index} data-window=${timer.tab.windowId} data-id=${key}>`;
      $el += `<td class='table-l'><p>${timer.tab.url}</p></td>`;
      $el += `<td class='table-s'>${calculateTime(timer.interval)}</td>`;
      $el += `<td class='table-m'><span class='duration-${timer.tab.index}'>${
        timer.duration ? timer.duration + " ms" : "Pending ..."
      }</span></td>`;
      $el += `<td class='table-s'><span class='remaining-${
        timer.tab.index
      }'>${calculateTime(timer.timeLeft._i)}</span></td>`;
      $el += `<td class='table-s'><i class="fas fa-external-link-alt" title="Switch to tab"></i><i class="fas fa-pause" title="Pause timer"></i></td></tr>`;
    }
    for (key in pausedTimers) {
      let timer = pausedTimers[key];
      $el += `<tr class='timer-row timer-${key}' data-tab=${timer.tab.index} data-window=${timer.tab.windowId} data-id=${key}>`;
      $el += `<td class='table-l'><p>${timer.tab.url}</p></td>`;
      $el += `<td class='table-s'>${calculateTime(timer.interval)}</td>`;
      $el += `<td class='table-m'><span class='duration-${timer.tab.index}'>${
        timer.duration ? timer.duration + " ms" : "Pending ..."
      }</span></td>`;
      $el += `<td class='table-s'><span class='remaining-${
        timer.tab.index
      }'>${calculateTime(timer.timeLeft._i)}</span></td>`;
      $el += `<td class='table-s'><i class="fas fa-external-link-alt" title="Switch to tab"></i><i class="fas fa-play" title="Play timer"></i></td></tr>`;
    }
    if ($el === "") {
      $el =
        "<tr class='timer-row'><td class='data-empty' colspan='5'>No timer running.</tr>";
    } else {
      $(".remove-all").removeClass("disabled");
    }
    $(".tr-load").remove();
    $("#data").append($el);
  }

  function updateTimersLive() {
    setInterval(function() {
      let shouldRerender = false;
      for (key in timers) {
        if (!(key in initialTimers)) {
          // there's a new timer!
          shouldRerender = true;
        }
      }
      for (key in initialTimers) {
        if (!(key in timers)) {
          // one timer has been removed!
          shouldRerender = true;
        }
      }
      if (shouldRerender) {
        renderTimers();
        initialTimers = JSON.parse(JSON.stringify(timers));
      }
    }, 5000);
  }

  $(".fa-external-link-alt, td:first-child").on("click", function(e) {
    let row = $(e.target).parents("tr")[0];
    let windowId = parseInt($(row).attr("data-window"));
    let tabs = parseInt($(row).attr("data-tab"));
    chrome.windows.update(windowId, {
      focused: true
    });
    chrome.tabs.highlight({
      tabs,
      windowId
    });
  });

  $("#data").on("click", ".fa-pause", function(e) {
    e.preventDefault();
    const row = $($(e.target).parents("tr")[0]);
    const id = parseInt($(row).attr("data-id"));
    pauseTimer(id, row);
  });

  $("#data").on("click", ".fa-play", function(e) {
    const row = $($(e.target).parents("tr")[0]);
    const id = parseInt($(row).attr("data-id"));
    runTimer(id, row);
  });

  $(".pause-all").on("click", function() {
    for (key in timers) {
      const row = $(`.timer-${key}`);
      pauseTimer(parseInt(key), row);
    }
  });

  $(".play-all").on("click", function() {
    for (key in pausedTimers) {
      const row = $(`.timer-${key}`);
      runTimer(parseInt(key), row);
    }
  });
  const blocker = document.querySelector("#block");
  const checked = localStorage.blocker ? localStorage.blocker : false;
  $("#block").prop("checked", checked === "false" ? false : true);
  blocker.addEventListener("click", function() {
    localStorage.blocker = document.querySelector("#block").checked;
  });

  function runTimer(id, row) {
    ext.timers.add(pausedTimers[id]);
    delete pausedTimers[id];
    row
      .find(".fa-play")
      .replaceWith(`<i class="fas fa-pause" title="Pause timer"></i>`);
  }

  function pauseTimer(id, row) {
    const timer = timers[id];
    timer.remainingTime = timer.nextRefresh - new Date().getTime();
    pausedTimers[id] = timer;
    ext.timers.remove(id);
    row
      .find(".fa-pause")
      .replaceWith('<i class="fas fa-play" title="Run timer"/>');
  }

  function calculateTime(i) {
    let duration = moment.duration(i);
    return `${duration._data.minutes}m ${duration._data.seconds}s`;
  }

  chrome.webRequest.onBeforeSendHeaders.addListener(
    details => {
      if (ext && ext._timers && ext._timers[details.tabId]) {
        reqAll[details.tabId] = details.url;
        reqStorage[details.tabId] = {
          startTime: new Date().getTime()
        };
      } else {
        if (document.querySelector("#block").checked) {
          for (const key in reqAll) {
            if (reqAll.hasOwnProperty(key)) {
              const element = reqAll[key];
              if (element === details.initiator + "/") {
                setTimeout(function() {
                  chrome.tabs.remove(details.tabId, function() {});
                }, 1);
                return { cancel: true };
              }
            }
          }
        }
      }
    },
    requestFilters,
    ["blocking"]
  );

  chrome.webRequest.onCompleted.addListener(details => {
    const { tabId } = details;
    if (reqStorage && reqStorage[tabId]) {
      reqStorage[tabId].endTime = new Date().getTime();
      ext._timers[tabId].duration =
        reqStorage[tabId].endTime - reqStorage[tabId].startTime;
    }
  }, requestFilters);
});
