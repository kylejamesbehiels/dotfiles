# Defined in - @ line 1
function brightness --wraps='brightnessctl s' --description 'alias brightness=brightnessctl s'
  brightnessctl s $argv;
end
